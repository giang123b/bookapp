package com.river.bookapp.data.local

import com.river.bookapp.data.repository.FavoriteDatabase
import com.river.bookapp.model.Favorite

class FavoriteBookRepository(private val db: FavoriteDatabase) {
    fun upsert(item: Favorite) = db.favoriteDao()?.insert(item)
    fun delete(item: Favorite) =
        db.favoriteDao()?.delete(item)

    fun getAllShoppingItems() = db.favoriteDao()?.selectallrecord()


}