package com.river.bookapp.data.remote

import com.river.bookapp.model.Programming
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {
    @GET("volumes")

    //Coroutine
    suspend fun getposts(@Query("q")q:String):Response<Programming>

}