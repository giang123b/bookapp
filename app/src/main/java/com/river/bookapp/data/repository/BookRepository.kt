package com.river.bookapp.data.repository

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.river.bookapp.data.remote.PostClient
import com.river.bookapp.model.Items
import com.river.bookapp.model.Programming
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class BookRepository : ViewModel() {
    var mutableLiveData = MutableLiveData<List<Items>>()
    fun onFailure(t: Throwable) {
    }

    fun onResponse(response: Programming) {
        val items: List<Items?> = response.items
        mutableLiveData.setValue(items as List<Items>?)
    }

    suspend fun getdata_book(book: String): MutableLiveData<List<Items>> {

        //Coroutine Kotlin
        viewModelScope.launch(Dispatchers.Main) {
            val responce= PostClient.getClient.getposts(book)
            val programming: Programming? =responce.body()
            val items: List<Items?> = programming!!.getitem()
            mutableLiveData.value=items as List<Items>

        }

        return withContext(Dispatchers.IO){
            mutableLiveData
        }
    }

    companion object {
        private const val TAG = "Repositry"
    }

}



