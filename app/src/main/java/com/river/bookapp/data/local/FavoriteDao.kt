package com.river.bookapp.data.local

import androidx.lifecycle.LiveData
import androidx.room.*
import com.river.bookapp.model.Favorite


@Dao
interface FavoriteDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(favorite: Favorite)

    @Update
    fun update(favorite: Favorite)

    @Delete
    fun delete(favorite: Favorite)

    @Query("DELETE FROM favorite_tab")
    fun deleteallrecord()

    @Query("SELECT * FROM favorite_tab")
    fun selectallrecord(): LiveData<List<Favorite>>

}