package com.river.bookapp.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.river.bookapp.R
import com.river.bookapp.view_model.BookViewModel
import com.river.bookapp.view_model.BookViewModelFactory
import com.river.bookapp.model.Favorite
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_show_detail_book.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class ShowDetailBookActivity : AppCompatActivity(), KodeinAware {

    lateinit var image: String
    lateinit var des: String
    lateinit var title: String
    lateinit var author: String

    private val factory: BookViewModelFactory by instance<BookViewModelFactory>()

    lateinit var viewModel: BookViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_detail_book)
        image = intent.getStringExtra("image").toString()
        des = intent.getStringExtra("des").toString()
        title = intent.getStringExtra("title").toString()
        if (intent.getStringExtra("author") == null) {
            author = "no author"
        } else {
            author = intent.getStringExtra("author").toString()

        }

        Picasso.get().load(image).into(header_image)
        textView.text = des
        viewModel = ViewModelProvider(this, factory).get(BookViewModel::class.java)

        fav.setOnClickListener {
            val favorite = Favorite(image, title, author, des)
            viewModel.insert(favorite)
            Toast.makeText(this, "save in Favorite", Toast.LENGTH_SHORT).show()
        }
    }

    override val kodein by kodein()
}