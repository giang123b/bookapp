package com.river.bookapp.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.river.bookapp.R
import com.river.bookapp.data.repository.BookRepository
import com.river.bookapp.model.SportsBook
import com.river.bookapp.view.adapter.BookProgramAdapter
import com.river.bookapp.view.adapter.AdapterOfSports
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class HomeFragment : Fragment() {

    lateinit var adapter: BookProgramAdapter
    lateinit var repositry: BookRepository
    lateinit var adapterOfSports: AdapterOfSports
    var listsportsbook: ArrayList<SportsBook> = ArrayList()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialization()

        GlobalScope.launch (Dispatchers.Main){
            repositry.getdata_book("programming").observe(viewLifecycleOwner, Observer { items ->
                adapter = BookProgramAdapter()
                adapter.setData(items)
                recycler_programming.adapter = adapter;
            })
        }

        addItem()
        adapterOfSports = AdapterOfSports()
        adapterOfSports.setData(listsportsbook)
        recycler_sports_book.adapter = adapterOfSports

    }

    private fun addItem() {
        listsportsbook.add(SportsBook(R.drawable.basketball, "Basketball"))
        listsportsbook.add(SportsBook(R.drawable.tennis, "Tennis"))
        listsportsbook.add(SportsBook(R.drawable.volleyball, "Volleyball"))
        listsportsbook.add(SportsBook(R.drawable.soccer, "Football"))

    }


    private fun initialization() {
        repositry = ViewModelProviders.of(this).get(BookRepository::class.java)

        recycler_programming.layoutManager = LinearLayoutManager(
            context,
            LinearLayoutManager.HORIZONTAL,
            false
        )
        recycler_sports_book.layoutManager = LinearLayoutManager(
            context,
            LinearLayoutManager.HORIZONTAL,
            false
        )

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

}