package com.river.bookapp.view

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.river.bookapp.R
import com.river.bookapp.view_model.BookViewModel
import com.river.bookapp.view_model.BookViewModelFactory
import com.river.bookapp.model.Favorite
import com.river.bookapp.view.adapter.AdapterOfFavorite
import kotlinx.android.synthetic.main.fragment_favorite_book.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.closestKodein
import org.kodein.di.generic.instance
import androidx.lifecycle.Observer

class FavoriteBookFragment : Fragment() , KodeinAware{
    override val kodein: Kodein by closestKodein()
    lateinit var viewModel: BookViewModel
    private val factory: BookViewModelFactory by instance<BookViewModelFactory>()
    lateinit var adapterOfFavorite: AdapterOfFavorite
    lateinit var list: List<Favorite>

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialization()

        viewModel.getAllShoppingItems().observe(viewLifecycleOwner, Observer { item ->
            list=item
            adapterOfFavorite.setData(item)
            rec_fav.adapter = adapterOfFavorite
            ItemTouchHelper(simpleCallBack).attachToRecyclerView(rec_fav)

        })
    }

    private fun initialization() {
        viewModel = ViewModelProvider(this, factory).get(BookViewModel::class.java)
        adapterOfFavorite = AdapterOfFavorite()
        rec_fav.layoutManager = LinearLayoutManager(
            context,
            LinearLayoutManager.VERTICAL,
            false
        )
    }

    companion object {
        private const val TAG = "FavoriteFragment"
    }

    private var simpleCallBack: ItemTouchHelper.SimpleCallback =
        object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT or ItemTouchHelper.LEFT) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val info_favorite = Favorite(
                    list.get(viewHolder.adapterPosition).image,
                    list.get(viewHolder.adapterPosition).title,
                    list.get(viewHolder.adapterPosition).author,
                    list.get(viewHolder.adapterPosition).des
                )
                viewModel.delete(info_favorite)
                adapterOfFavorite.notifyDataSetChanged()
            }
        }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_favorite_book, container, false)
    }

}