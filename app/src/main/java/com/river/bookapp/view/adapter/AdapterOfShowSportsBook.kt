package com.river.bookapp.view.adapter

import android.app.Application
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.river.bookapp.R
import com.river.bookapp.data.repository.BookRepository
import com.river.bookapp.model.Items
import com.river.bookapp.view.ShowDetailBookActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.layout_show_sports_book.view.*

class AdapterOfShowSportsBook() :
    RecyclerView.Adapter<AdapterOfShowSportsBook.ViewHolder>() {
    lateinit var list: List<Items>
    lateinit var repositry: BookRepository
    lateinit var application: Application
    lateinit var mContext: Context

    fun setData(data: List<Items>) {
        list = data
    }

    fun setRepository(repository: BookRepository) {
        this.repositry = repository
    }

    fun setApp(application: Application) {
        this.application = application
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun find(item: Items) {
            Picasso.get().load(item.bookInformation.imageLinks.thumbnail).into(itemView.image_show_book)
            itemView.title_show_book.text = item.bookInformation.authors[0].toString()
            itemView.name_show_book.text = item.bookInformation.title
        }

    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {

        mContext = parent.context
        return ViewHolder(
            LayoutInflater.from(
                mContext
            ).inflate(
                R.layout.layout_show_sports_book,
                parent,
                false
            )
        )
    }


    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val items = list[position]
        holder.find(items)
        var title: String
        var des: String
        var author: String
        holder.itemView.setOnClickListener {
            val intent = Intent(mContext, ShowDetailBookActivity::class.java)
            intent.putExtra("image", items.bookInformation.imageLinks.thumbnail)
            title = items.bookInformation.title
            author = items.bookInformation.authors.get(0).toString()
            intent.putExtra("author", author)
            title = items.bookInformation.title
            intent.putExtra("title", title)
            intent.putExtra("des", items.bookInformation.description.toString())
            des = items.bookInformation.description
            mContext.startActivity(intent)
        }


    }
}