package com.river.bookapp.view

import android.app.Application
import com.river.bookapp.data.local.FavoriteBookRepository
import com.river.bookapp.view_model.BookViewModelFactory
import com.river.bookapp.data.repository.FavoriteDatabase
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton


class BookApplication : Application(), KodeinAware {
    override val kodein: Kodein = Kodein.lazy {
        import(androidXModule(this@BookApplication))
        bind() from singleton { FavoriteDatabase(instance()) }
        bind() from singleton {
            FavoriteBookRepository(
                instance()
            )
        }
        bind() from provider {
            BookViewModelFactory(
                instance()
            )
        }
    }
}