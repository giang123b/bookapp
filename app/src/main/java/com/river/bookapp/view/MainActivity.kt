package com.river.bookapp.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.ismaeldivita.chipnavigation.ChipNavigationBar
import com.river.bookapp.R
import com.river.bookapp.view.adapter.BookProgramAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    lateinit var adapter: BookProgramAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportFragmentManager.beginTransaction().replace(R.id.framelayout, HomeFragment()).commit()

        chipNavigationBar.setOnItemSelectedListener(object :
            ChipNavigationBar.OnItemSelectedListener {
            override fun onItemSelected(id: Int) {
                selectFragment(id)
            }

        }

        )
    }

    private fun selectFragment(id: Int) {
        var fragment: Fragment? = null
        if (id == R.id.home)
            fragment = HomeFragment()
        else if (id == R.id.favorite)
            fragment = FavoriteBookFragment()

        if (fragment != null) {
            supportFragmentManager.beginTransaction().replace(R.id.framelayout, fragment).commit()
        }
    }
}