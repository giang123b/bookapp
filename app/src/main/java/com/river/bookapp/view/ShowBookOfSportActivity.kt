package com.river.bookapp.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.river.bookapp.R
import com.river.bookapp.data.repository.BookRepository
import com.river.bookapp.view.adapter.AdapterOfShowSportsBook
import kotlinx.android.synthetic.main.activity_show_book_of_sport.*
import kotlinx.android.synthetic.main.activity_show_detail_book.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class ShowBookOfSportActivity : AppCompatActivity() {

    lateinit var adapterOfShowSportsBook: AdapterOfShowSportsBook
    lateinit var repositry: BookRepository
    lateinit var categrySports: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_book_of_sport)

        initialization()
        categrySports = intent.getStringExtra("name").toString()
        GlobalScope.launch(Dispatchers.Main) {
            repositry.getdata_book(categrySports)
                .observe(this@ShowBookOfSportActivity, Observer { item ->
                    adapterOfShowSportsBook.setData(item)
                    recycler_show_sports_book.adapter = adapterOfShowSportsBook
                })
        }

        back.setOnClickListener {
            finish()
        }
        name_sports_show.text = categrySports
    }


    private fun initialization() {
        repositry = ViewModelProviders.of(this).get(BookRepository::class.java)
        adapterOfShowSportsBook = AdapterOfShowSportsBook()
        adapterOfShowSportsBook.setRepository(repositry)
        adapterOfShowSportsBook.setApp(application)
        recycler_show_sports_book.layoutManager = LinearLayoutManager(
            this,
            LinearLayoutManager.VERTICAL,
            false
        )
    }

}