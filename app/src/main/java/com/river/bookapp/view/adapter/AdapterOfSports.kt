package com.river.bookapp.view.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.river.bookapp.R
import com.river.bookapp.model.SportsBook
import com.river.bookapp.view.ShowBookOfSportActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.layout_sports_book.view.*

class AdapterOfSports() : RecyclerView.Adapter<AdapterOfSports.ViewHolder>() {

    lateinit var mContext: Context
    lateinit var listSportsBook: List<SportsBook>

    fun setData(list: List<SportsBook>) {
        listSportsBook = list
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun find(sportsBook: SportsBook) {
            Picasso.get().load(sportsBook.image_Book).into(itemView.image_sports)
            itemView.name_sports.text = sportsBook.title_book
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mContext = parent.context
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.layout_sports_book, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return listSportsBook.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val items = listSportsBook[position]
        holder.find(items)

        holder.itemView.setOnClickListener {
            val intent=Intent(mContext,
                ShowBookOfSportActivity::class.java)
            intent.putExtra("name",items.title_book)
            mContext.startActivity(intent)
        }
    }
}