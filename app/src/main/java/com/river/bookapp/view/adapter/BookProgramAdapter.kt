package com.river.bookapp.view.adapter

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.river.bookapp.R
import com.river.bookapp.model.Items
import com.river.bookapp.view.ShowDetailBookActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.layout_programing_book.view.*


class BookProgramAdapter() : RecyclerView.Adapter<BookProgramAdapter.ViewHolder>() {

    lateinit var list: List<Items>
    lateinit var mContext: Context
    fun setData(data: List<Items>) {
        list = data
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun find(item: Items) {

            Picasso.get().load(item.bookInformation.imageLinks.thumbnail + "").into(itemView.image_prog)
            itemView.title_prog.text = item.bookInformation.authors.get(0).toString()
            itemView.name_prog.text = item.bookInformation.title

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        mContext = parent.context
        return ViewHolder(
            LayoutInflater.from(
                mContext
            ).inflate(
                R.layout.layout_programing_book,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val items = list[position]
        holder.find(items)
        holder.itemView.setOnClickListener {
            val intent = Intent(mContext, ShowDetailBookActivity::class.java)
            intent.putExtra("image", items.bookInformation.imageLinks.thumbnail + "")
            intent.putExtra("title", items.bookInformation.title.toString())
            intent.putExtra("author", items.bookInformation.authors.get(0).toString())
            intent.putExtra(
                "des",
                items.bookInformation.description.toString()
            )

            mContext.startActivity(intent)
        }
    }
}