package com.river.bookapp.view.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.river.bookapp.R
import com.river.bookapp.model.Favorite
import com.river.bookapp.view.ShowDetailBookActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.layout_show_sports_book.view.*

class AdapterOfFavorite() :
    RecyclerView.Adapter<AdapterOfFavorite.ViewHolder>() {
    lateinit var list: List<Favorite>
    lateinit var mContext: Context
    fun setData(data: List<Favorite>) {
        list = data
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun find(item: Favorite) {
            Picasso.get().load(item.image).into(itemView.image_show_book)
            itemView.title_show_book.text = item.author.toString()
            itemView.name_show_book.text = item.title
        }

    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        mContext = parent.context
        return ViewHolder(
            LayoutInflater.from(
                mContext
            ).inflate(
                R.layout.layout_show_sports_book,
                parent,
                false
            )
        )
    }


    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val items = list[position]
        holder.find(items)
        var title: String
        var des: String
        var author: String
        holder.itemView.setOnClickListener {
            val intent = Intent(mContext, ShowDetailBookActivity::class.java)
            intent.putExtra("image", items.image)
            title = items.title

            author = items.author
            intent.putExtra("author", author)
            title = items.title
            intent.putExtra("title", title)

            intent.putExtra("des", items.des)
            des = items.des
            mContext.startActivity(intent)
        }


    }
}