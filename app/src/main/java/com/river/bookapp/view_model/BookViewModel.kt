package com.river.bookapp.view_model

import androidx.lifecycle.ViewModel
import com.river.bookapp.data.local.FavoriteBookRepository
import com.river.bookapp.model.Favorite

class BookViewModel(
    private val repository: FavoriteBookRepository
) : ViewModel() {
    fun insert(item: Favorite) {
        repository.upsert(item)
    }

     fun delete(item: Favorite){
        repository.delete(item)
    }

    fun getAllShoppingItems() = repository.getAllShoppingItems()

}