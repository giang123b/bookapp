package com.river.bookapp.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Items(
    @SerializedName("volumeInfo")
    @Expose
    var bookInformation: BookInformation
    )


