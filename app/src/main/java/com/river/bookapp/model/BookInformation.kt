package com.river.bookapp.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class BookInformation(
    @SerializedName("title")
    @Expose var title: String,

    @SerializedName("authors")
    @Expose
     val authors: ArrayList<*>,

    @SerializedName("description")
    @Expose
     val description: String,

    @SerializedName("imageLinks")
    @Expose
    var imageLinks: ImageLink
)