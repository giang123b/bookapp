package com.river.bookapp.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ImageLink (
    @SerializedName("thumbnail")
    @Expose
    public var thumbnail: String
)