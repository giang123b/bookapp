package com.river.bookapp.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlin.collections.ArrayList

class Programming(
    @SerializedName("items")
    @Expose
    var items: ArrayList<Items?>
) {

   fun getitem(): ArrayList<Items?> {
       return items
   }
}