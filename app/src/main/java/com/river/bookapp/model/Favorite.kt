package com.river.bookapp.model

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "favorite_tab")
class Favorite() {
    lateinit var image: String

    @PrimaryKey
    @NonNull
    lateinit var title: String

    lateinit var author: String

    lateinit var des: String

    constructor(image: String, title: String, author: String, des: String) : this() {
        this.image = image
        this.title = title
        this.author = author
        this.des = des
    }


}

