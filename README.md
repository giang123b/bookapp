# BookIntroduce
Simple Android app to show information of Book

## PREVIEW
![All screen](/app/src/main/res/drawable/image_all_screen.jpg?raw=true)

### Android
This application implements :
- MVVM 
- ViewModel
- Coroutines
- Retrofit
- Room
- RecyclerView
